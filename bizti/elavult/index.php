<?php
$ID = uniqid();
mkdir("temp/" . $ID);
$target_dir = "temp/" ;
$target_file = $target_dir . "/" . $ID . "/" . basename($_FILES["fileToUpload"]["name"]);
$target = basename($target_file,".docx");
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "docx") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

// rename
    rename($target_file, "temp/" .$ID . "/"  . $target .'.zip');
// create folder
mkdir("docs/" . $ID . "/" . $target);


// assuming file.zip is in the same directory as the executing script.
$file = "temp/" . $ID . "/" . $target .'.zip';

// get the absolute path to $file
$path = "docs/" . $ID . "/" . $target;

$zip = new ZipArchive;
$res = $zip->open($file);
if ($res === TRUE) {
  // extract it to the path we determined above
  $zip->extractTo($path);
  $zip->close();
  echo "WOOT! $file extracted to $path";
} else {
  echo "Doh! I couldn't open $file";
}
$ekker = file_get_contents('docs/' . $ID . "/" . $target . '/docProps/custom.xml');
if (preg_match_all ('_name="(.*?)"_',  $ekker, $tomb2));
if (preg_match_all ('_<vt:lpwstr>(.*?)</vt:lpwstr>_',  $ekker, $tomb));


include 'values.html';
?>